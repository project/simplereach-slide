Drupal.behaviors.srslide = function(context) {
  __spr_config = Drupal.settings.srslide;
  if(!__spr_config.css){
    __spr_config.css = document.location.protocol + '//d8rk54i4mohrb.cloudfront.net/css/p/' + __spr_config.pid + '.css';
  }
  (function(){
    var s = document.createElement('script');
    s.async = true;
    s.type = 'text/javascript';
    s.src = document.location.protocol + '//d8rk54i4mohrb.cloudfront.net/js/slide.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(s);
})();
}