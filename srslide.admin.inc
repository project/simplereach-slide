<?php

/**
 * @file
 * Provides the administration settings for the SimpleReach Slide Drupal module.
 */

/**
 * Implements hook_form().
 */
function srslide_admin_settings() {
  $form = array();
  $form['contact'] = array(
    '#value' => t("If you have any questions, please don't hesitate to ") . l(t('contact us'), 'https://www.simplereach.com/contact') . ".",
  );
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account Settings'),
  );
  $form['account']['srslide_pid'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID (PID)'),
    '#description' => t('If you need help finding your PID, please click here.'),
    '#default_value' => variable_get('srslide_pid', ''),
    '#required' => TRUE,
  );

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings'),
  );
  $form['display']['srslide_header'] = array(
    '#type' => 'textfield',
    '#title' => t('Edit Header (Max 30 char)'),
    '#description' => t('All letters will be uppercase. (Default: RECOMMENDED FOR YOU)'),
    '#default_value' => variable_get('srslide_header', 'RECOMMENDED FOR YOU'),
    '#size' => 30,
    '#maxlength' => 30,
    '#attributes' => array('style' => 'text-transform: uppercase;'),
  );
  $form['display']['srslide_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display The Slide logo (increases engagement)'),
    '#default_value' => variable_get('srslide_logo', 1),
  );
  $form['display']['srslide_css'] = array(
    '#type' => 'textfield',
    '#title' => t('The Slide CSS'),
    '#description' => t('The absolute URL (http://example.com/slide.css) to your hosted CSS file. If you have any questions, click ') . l(t('here'),'https://www.simplereach.com/docs/publisher/slide_style') . ".",
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('srslide_css', ''),
  );

  return system_settings_form($form);
}

